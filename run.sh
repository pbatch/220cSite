OUTDIR=_site

if [ "$#" -eq 0 ]
then
echo "Usage: sh run.sh [build|clean|transfer] (ROOT)"
exit
fi

ROOT="."

if [ "$#" -eq 2 ] 
then
    ROOT=$2
fi

if [ "$1" == "build" ]
then
    echo "Building..."
    find -type d | egrep -v "^./_" | sed "s!^\./!$OUTDIR/!" | xargs -n 1 mkdir -p
    find -name "*.md" | egrep -v "^./_" | sed "s!^\./!!" | xargs -n 1 ./sitegen -r $ROOT $OUTDIR

    rsync -rvt res $OUTDIR
    rsync -rvt css $OUTDIR

elif [ "$1" == "clean" ]
then
    echo "Cleaning..."
    rm -rf $OUTDIR

elif [ "$1" == "transfer" ]
then
    echo "Transfering to _live..."
    rsync -rvt _site/* _live

else
    echo "Error: invalid command $1"
    echo "Run again without any commands to see a list of options."

fi

