# Spigot

Spigot is a set of simple graphical interfaces for 
[Sporth](https://paulbatchelor.github.io/proj/sporth.html). 
It is available
for download on [github](https://www.github.com/paulbatchelor/spigot) and
[CCRMA-hosted gitlab](https://cm-gitlab.stanford.edu/pbatch/Spigot/).


## How Spigot Works

Spigot is loaded inside of Sporth as a plugin in the following way:

    _spigot "./spigot.so" fl
    0 120 4 clock 1 "foo.rnt" _spigot fe
    # Do stuff here.
    _spigot fc

The spigot executable takes in 3 parameters in the following order,
left to right:

- A clock signal 
- A init-time boolean parameter for display. When it is 1, the graphics
will display normally. When it is 0, the graphics will be disabled and 
Spigot will run in headless mode for things like offline rendering.
- A filename containing a location of a configuration file, written
using "runt". More info on configuration files Later.

The clock signal drives the various sequencers and programs in Spigot. When
triggered, it causes Spigot to step. 

The Spigot canvas is 193x193 pixels (or a 12x12 8x8 tileset plus 1pixel for
borders). It uses a 24-bit RGB colorspace. The frame typically gets redrawn 
any time the clock signal triggers or a key input is pressed. There is
a zoom factor which allows magnification.

Graphics in Spigot are spawned inside of Sporth, and they can close and
open again when using Sporth as a live coding environment. 

Spigot has a number of standard key combos so multiple interfaces can share
the same key bindings:

- HJKL: Vi movement (up, down, left, right)
- Space: start/stop

For configuration files, Spigot uses [Runt](https://paulbatchelor.github.io/proj/runt), a stack-based 
language. This tells Spigot what to load and how to load. Most importantly,
Runt files are needed to select which interface you'd like to use, called 
a Spigot *state*.

## Spigot States

In Spigot, a *state* is a particular predefined musical interface using 
a generaized internal C interface provided by Spigot. At the moment, there
are only two interfaces made, but there is potential for growth.

### Tracker

![tracker]({{ROOT}}/res/spigot/tracker.png)

The Spigot state **tracker** is a [music-tracker](
https://en.wikipedia.org/wiki/Music_tracker) that builds on 
[previous work]( 
https://paulbatchelor.github.io/blog/posts/2017-03-26-bitmap_interfaces.html) with 
[trackers](https://paulbatchelor.github.io/proj/protrekkr) in Sporth.


A typical runt file for **tracker** could look like this:

    new_state tracker spigot_state

    # open file for read/write
    "ex/notes.rnt" tracker_open

    # set zoom level to 3
    2 spigot_zoom 

    # Create Sporth table for note data
    "notes" tracker_notes

    # Create Sporth table for gate data
    "gates" tracker_gates

    # Create variable that is a gate for start/stop
    "play" tracker_play

Here is a simple Sporth file that reads note data from channel 1 (leftmost):


    _spigot "./spigot.so" fl
    _clk var

    # Create Clock 
    4 metro dup _clk set 
    1 "ex/tracker.rnt" _spigot fe 


    # Give me a sine... 
    0 _notes tget mtof 0.3 sine 
    # Put it through a filterted gate signal
    0 _gates tget 0.01 port * 

    # multiply whole signal with play toggle
    _play get *
    dup
    _spigot fc

### Pbrain

![Pbrain]({{ROOT}}/res/spigot/pbrain.png)

The Spigot state **pbrain** is a musical sequencer based on the pbrain 
language (a dialect of brainfuck). A "score" of BF code is parsed and drawn
onto a grid. Every clock tick steps through the code, outputing the current
symbol to a variable sporth. This variable can be used as a control signal for 
things like sequencers and values.

A simple runt file for pbrain could look like this:

    new_state "++[..-]" "output" pbrain spigot_state

And the corresponding Sporth file:


    _spigot "./spigot.so" fl
    _notes "60 60 62 64 67 69 71 72 74" gen_vals
    _val var
    _clk var
    _env var

    0.3 dmetro _clk set

    _clk get 1


    "ex/ex.rnt"

    _spigot fe 

    _clk get _output get * 0.001 0.01 0.1 tenvx _env set
    _env get _output get 
    dup 0 ne samphold 
    _notes tget mtof 0.3 
    1 7 _env get 1 *  fm *

    dup
    _spigot fc


