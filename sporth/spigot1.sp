##: # Spigot Demo 1
##: This sporth patch is a simple demonstration of *Spigot*, a sporth
##: plugin that is both a sequencer and interpretor for the pbrain language.
##: This reads from a list of example pbrain strings, and will sonifies the 
##: eight core instructions.

##: ## Setup
##: ### Loading up the examples
##: A set of strings (read line-by-line in "examples.txt" ) are loaded into a 
##: string-list *list* via **slist**. **slist** statically allocates an array,
##: so a reasonably large value of 10 is used (at present, there are only
##: 3 example strings).

##---
_list 10 "examples.txt" slist
##---

##: ### Loading the plugin
##: The spigot custom ugen is loaded up via **fl**. The assumption here is that
##: the shared object file is in the current directory. 
##---
_spigot "./spigot.so" fl
##---

##: ### Tables and Variables
##: A number of tables and variables are initialized. 
##: - The table *notes* creates a table of MIDI note numbers, one for each
##: instruction. Since the instructions are not zero indexed, an extra value
##: is appened at the beginning 
##: - The variable *val* will be used to store the signal generated from
##: spigot.
##: - The variable *clk* will store the clock signal. 
##: - The variable *env* will store the envelope signal. 
##---
_notes "60 60 62 64 67 69 71 72 74" gen_vals
_val var
_clk var
_env var
##---

##: ## Runtime
##: ### Clock
##: The clock signal is the first signal to be generated in the patch. It 
##: will control the speed of the spigot virtual CPU. The clock it set to tick
##: once every 300ms via **dmetro**. This slow rate is done for do demonstration
##: purposes, so it is easy to hear what instruction it is on.
##---
0.3 dmetro _clk set
##---

##: ### Instantiating Spigot
##: An instance of Spigot is created via **fe**. The arguments that 
##: spigots take are an input signal (which spigot can optionally read),
##: and the clock signal *clk*.
##---
# Input Signal
0 
# Clock Signal
_clk get 
##---

##: Spigot parses a string rather than a file. The string is selected from
##: the slist *list* via **sget**. 
##---
2 _list sget
_spigot fe 
##---
##: The output of spigot is the current instruction executed (if triggered), 
##: otherwise it is zero. This value is stored in the variable *val*.
##---
_val set
##---

##: ### Envelope
##: Next, the envelope signal is generated via **tenvx**. Recall that 
##: a trigger in a clock signal is a sample that is non-zero. Because of this,
##: *val* is able to be used as a clock signal
##---
_val get 0.001 0.01 0.01 tenvx 
##---

##: The signal of the envelope is set to *env*.
##---
_env set
##---
##: It is immediately retrieved for later. Probably should have used **dup** 
##: here.
##---
_env get 
##---

##: ### Setting the Frequency
##: The frequency of the oscillator is determined by the output of spigot, so
##: it is retrieved from *val*.
##---
_val get 
##---

##: To better grok the instructions, the value is printed to the terminal.
##---
"val" print 
##---

##: The signal is duplicated and turned into a specific trigger signal 
##: using the **ne** (not equals) operator. These two values feed a 
##: sample and hold generator **samphold**. This is needed because the 
##: output of spigot is essentially a trigger signal and not a step signal.
##---
dup 0 ne samphold 
##---
##: The instructions correspond to indices in the table *notes*, which we
##: access via **tget**. These are then converted to a frequency via **mtof**.
##---
_notes tget mtof 
##---

##: Frequency is the first argument needed for the FM oscillator, which 
##: is used for sound generation.

##: ### The FM oscillator
##: Amplitude, and the carrier/modulator ratios are set. These are just
##: constants. The C:M ratio is 1:7, a very common ratio used for FM tine piano
##: sounds.
##: 
##---
0.3 1 7 
##---
##: The modulation index uses the envelope stored in *inv* as a control 
##: parameter. This effectively modulations the brightness of the FM oscillator.
##---
_env get 1 * 
##---

##: The FM oscillator is then multiplied by the envelope, which was pushed
##: on the stack previously. This signal is then duplicated to make a stereo
##: patch
##---
fm * dup
##---
##: ## Closing
##: The sporth plugin is closed via **fc**. Not necessary, but the nice thing
##: to do.
##---
_spigot fc
##---
