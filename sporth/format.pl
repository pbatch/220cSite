#!/usr/bin/perl
use 5.01;
use strict;

my $codemode=0;
my $openfile= 1;
my $out;

print(
"\n\\[[home]({{ROOT}}/)]".
" \\[[patches]({{ROOT}}/sporth)]\n\n"
);

while(<>)
{
    if(m/^##:/) {
        chomp;
        s/^##:\s*//;

        print $_."\n";

        if(m/\# (.*)/ and $openfile == 1) {
            my $name = lc($1);
            $name =~ s/ /_/g;
            $out = "res/sporth/$name.sp";
            open CODE, ">$out";
            $openfile = 0;
        }
    }
    if(m/^##---/) {
        if($codemode) {
            $codemode = 0;
            print("\n");
        } else {
            $codemode = 1;
            print("\n");
        }
    } elsif($codemode) {
        print "    $_";
        print CODE $_;
    }

}

print("\n\\[[code]({{ROOT}}/$out)]\n\n");
