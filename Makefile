PAGES=

default: gen

include sporth/Makefile

ROOT?="\/~pbatch\/220c"
#ROOT?="\/"

gen: $(PAGES)
	sh run.sh build $(ROOT)

transfer:
	sh run.sh transfer

listen:
	go run listen.go 

clean:
	sh run.sh clean
	rm -rf $(PAGES)
